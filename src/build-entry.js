import GLSButton from './components/GLSButton.vue'

export default {
  install: (app, options) => {
    app.component('gls-button', GLSButton)
  },
}
